FROM maven:3.9.0-eclipse-temurin-11-alpine as maven
WORKDIR /build_app
COPY . /build_app
RUN mvn clean install -Dmaven.test.skip=true
#RUN sh settings_db.sh

FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine-slim
WORKDIR /app
COPY --from=maven /build_app/target/*.jar /app/app.jar
RUN apk update --no-cache && apk add --no-cache curl
EXPOSE 8080
HEALTHCHECK --interval=20s --timeout=3s \
  CMD curl -f http://127.0.0.1:8080/student/getAll || exit 1
CMD ["java","-jar","/app/app.jar"]
